/*
 * Copy me if you can.
 * by 20h
 */

#define _BSD_SOURCE
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <strings.h>
#include <sys/time.h>
#include <time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>

#include <X11/Xlib.h>

char *tz_local = "America/Los_Angeles";

static Display *dpy;

int run_shell_cmd(const char *cmd, char *output, size_t output_size)
{
    FILE *proc = popen(cmd, "r");
    if(!proc)
        return -1;

    if(!fgets(output, sizeof(char) * output_size, proc))
        return -1;

    return pclose(proc);
}

char * smprintf(char *fmt, ...)
{
    va_list fmtargs;
    char *ret;
    int len;

    va_start(fmtargs, fmt);
    len = vsnprintf(NULL, 0, fmt, fmtargs);
    va_end(fmtargs);

    ret = malloc(++len);
    if (ret == NULL) {
        perror("malloc");
        exit(1);
    }

    va_start(fmtargs, fmt);
    vsnprintf(ret, len, fmt, fmtargs);
    va_end(fmtargs);

    return ret;
}

void settz(char *tzname)
{
    setenv("TZ", tzname, 1);
}

char * mktimes(char *fmt, char *tzname)
{
    char buf[129];
    time_t tim;
    struct tm *timtm;

    settz(tzname);
    tim = time(NULL);
    timtm = localtime(&tim);
    if (timtm == NULL)
        return smprintf("");

    if (!strftime(buf, sizeof(buf)-1, fmt, timtm)) {
        fprintf(stderr, "strftime == 0\n");
        return smprintf("");
    }

    return smprintf("%s", buf);
}

char * loadavg(void)
{
    double avgs[3];

    if (getloadavg(avgs, 3) < 0)
        return smprintf("");

    //return smprintf("%.2f %.2f %.2f", avgs[0], avgs[1], avgs[2]);
    return smprintf("%.2f", avgs[1]);
}

char * readfile(char *base, char *file)
{
    char *path, line[513];
    FILE *fd;

    memset(line, 0, sizeof(line));

    path = smprintf("%s/%s", base, file);
    fd = fopen(path, "r");
    free(path);
    if (fd == NULL)
        return NULL;

    if (fgets(line, sizeof(line)-1, fd) == NULL)
        return NULL;
    fclose(fd);

    return smprintf("%s", line);
}

char * getbattery(char *base)
{
    char *co, status;
    int descap, remcap;

    descap = -1;
    remcap = -1;

    co = readfile(base, "present");
    if (co == NULL)
        return smprintf("");
    if (co[0] != '1') {
        free(co);
        return smprintf("not present");
    }
    free(co);

    co = readfile(base, "charge_full_design");
    if (co == NULL) {
        co = readfile(base, "energy_full_design");
        if (co == NULL)
            return smprintf("");
    }
    sscanf(co, "%d", &descap);
    free(co);

    co = readfile(base, "charge_now");
    if (co == NULL) {
        co = readfile(base, "energy_now");
        if (co == NULL)
            return smprintf("");
    }
    sscanf(co, "%d", &remcap);
    free(co);

    co = readfile(base, "status");
    if (!strncmp(co, "Discharging", 11)) {
        status = '-';
    } else if(!strncmp(co, "Charging", 8)) {
        status = '+';
    } else {
        status = '?';
    }

    if (remcap < 0 || descap < 0)
        return smprintf("invalid");

    return smprintf("%.0f%%%c", ((float)remcap / (float)descap) * 100, status);
}

char * gettemperature(char *base, char *sensor)
{
    char *co;

    co = readfile(base, sensor);
    if (co == NULL)
        return smprintf("");
    return smprintf("%02.0f°C", atof(co) / 1000);
}

void setstatus(char *str)
{
    XStoreName(dpy, DefaultRootWindow(dpy), str);
    XSync(dpy, False);
}

void remove_newline(char *buffer, size_t length)
{
    for(int i = 0; i < length; ++i)
    {
        if(buffer[i] == '\n')
        {
            buffer[i] = '\0';
        }
    }
}

int main(void)
{
    char *status;
    char *avgs;
    char *bat;
    char *t_local;
    char *t0, *t1, *t2;
    char volume[10];
    char muted[10];
    char ip_address[16];
    char wifi_name[20];

    if (!(dpy = XOpenDisplay(NULL))) {
        fprintf(stderr, "dwmstatus: cannot open display.\n");
        return 1;
    }

    for (;;sleep(60)) {
        avgs = loadavg();
        bat = getbattery("/sys/class/power_supply/BAT1");
        t_local = mktimes("%Y-%m-%d %H:%M", tz_local);
        t0 = gettemperature("/sys/devices/virtual/hwmon/hwmon0", "temp1_input");
        t1 = gettemperature("/sys/devices/virtual/hwmon/hwmon2", "temp1_input");
        t2 = gettemperature("/sys/devices/virtual/hwmon/hwmon4", "temp1_input");
        run_shell_cmd("amixer -M sget Master | awk -F'[][]' '/Mono:/ { print $2 }'", volume, 10);
        run_shell_cmd("amixer -M sget Master | awk -F'[][]' '/Mono:/ { print $6 }'", muted, 10);
        run_shell_cmd("iwctl station wlan0 show | awk '/Connected network/ { print $3 }'", wifi_name, 20);
        run_shell_cmd("iwctl station wlan0 show | awk '/IPv4/ { print $3 }'", ip_address, 16);
        remove_newline(volume, 10);
        remove_newline(wifi_name, 20);
        remove_newline(ip_address, 16);
        if(!strncmp(volume, "100", 3))
            strncpy(volume, "MAX", 4);
        if(!strncmp(volume, "0%", 2) || !strncmp(muted, "off", 3))
            strncpy(volume, "---", 4);

        status = smprintf("[IP %s] [WIFI %s] [VOL %s] [BATT %s] [%s]", ip_address, wifi_name, volume, bat, t_local);
        setstatus(status);

        free(t0);
        free(t1);
        free(t2);
        free(avgs);
        free(bat);
        free(t_local);
        free(status);
    }

    XCloseDisplay(dpy);

    return 0;
}

